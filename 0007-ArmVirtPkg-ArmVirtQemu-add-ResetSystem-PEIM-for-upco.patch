From 1cb4d8d12542e95274881c7fce1c95816bd883ff Mon Sep 17 00:00:00 2001
From: Ard Biesheuvel <ard.biesheuvel@linaro.org>
Date: Wed, 26 Feb 2020 20:05:10 +0100
Subject: [PATCH 07/13] ArmVirtPkg/ArmVirtQemu: add ResetSystem PEIM for
 upcoming TPM2 support

As a first step in gradually adding TPM2 support to ArmVirtQemu, add
the TPM2_ENABLE configurable to the [Defines] section, and if it is
set, add the ResetSystem PEIM to the build, along with the library
class references that we will need to support it:
- wire ArmVirtPsciResetSystemPeiLib into the ResetSystem PEIM itself,
  which will be in charge of performing the actual reset
- add PeiResetSystemLib as the common ResetSystemLib resolution for
  PEIM class modules, so that other PEIMs will invoke the PPI
  published by the ResetSystem PEIM.

Ref: https://bugzilla.tianocore.org/show_bug.cgi?id=2560
Signed-off-by: Ard Biesheuvel <ard.biesheuvel@linaro.org>
Reviewed-by: Laszlo Ersek <lersek@redhat.com>
Signed-off-by: jiangfangjie <jiangfangjie@huawei.com>
---
 ArmVirtPkg/ArmVirtQemu.dsc | 14 ++++++++++++++
 ArmVirtPkg/ArmVirtQemu.fdf |  4 ++++
 2 files changed, 18 insertions(+)

diff --git a/ArmVirtPkg/ArmVirtQemu.dsc b/ArmVirtPkg/ArmVirtQemu.dsc
index 984df5c..3bbc79c 100644
--- a/ArmVirtPkg/ArmVirtQemu.dsc
+++ b/ArmVirtPkg/ArmVirtQemu.dsc
@@ -29,6 +29,7 @@
   #
   DEFINE TTY_TERMINAL            = FALSE
   DEFINE SECURE_BOOT_ENABLE      = FALSE
+  DEFINE TPM2_ENABLE             = FALSE
 
   #
   # Network definition
@@ -77,6 +78,10 @@
 [LibraryClasses.common.PEIM]
   ArmVirtMemInfoLib|ArmVirtPkg/Library/QemuVirtMemInfoLib/QemuVirtMemInfoPeiLib.inf
 
+!if $(TPM2_ENABLE) == TRUE
+  ResetSystemLib|MdeModulePkg/Library/PeiResetSystemLib/PeiResetSystemLib.inf
+!endif
+
 [LibraryClasses.common.DXE_DRIVER]
   ReportStatusCodeLib|MdeModulePkg/Library/DxeReportStatusCodeLib/DxeReportStatusCodeLib.inf
 
@@ -100,6 +105,8 @@
 
   gEfiMdeModulePkgTokenSpaceGuid.PcdTurnOffUsbLegacySupport|TRUE
 
+  gArmVirtTokenSpaceGuid.PcdTpm2SupportEnabled|$(TPM2_ENABLE)
+
 [PcdsFixedAtBuild.common]
 !if $(ARCH) == AARCH64
   gArmTokenSpaceGuid.PcdVFPEnabled|1
@@ -266,6 +273,13 @@
 
   MdeModulePkg/Universal/Variable/Pei/VariablePei.inf
 
+!if $(TPM2_ENABLE) == TRUE
+  MdeModulePkg/Universal/ResetSystemPei/ResetSystemPei.inf {
+    <LibraryClasses>
+      ResetSystemLib|ArmVirtPkg/Library/ArmVirtPsciResetSystemPeiLib/ArmVirtPsciResetSystemPeiLib.inf
+  }
+!endif
+
   MdeModulePkg/Core/DxeIplPeim/DxeIpl.inf {
     <LibraryClasses>
       NULL|MdeModulePkg/Library/LzmaCustomDecompressLib/LzmaCustomDecompressLib.inf
diff --git a/ArmVirtPkg/ArmVirtQemu.fdf b/ArmVirtPkg/ArmVirtQemu.fdf
index 2c8936a..69fa501 100644
--- a/ArmVirtPkg/ArmVirtQemu.fdf
+++ b/ArmVirtPkg/ArmVirtQemu.fdf
@@ -113,6 +113,10 @@ READ_LOCK_STATUS   = TRUE
   INF MdeModulePkg/Universal/Variable/Pei/VariablePei.inf
   INF MdeModulePkg/Core/DxeIplPeim/DxeIpl.inf
 
+!if $(TPM2_ENABLE) == TRUE
+  INF MdeModulePkg/Universal/ResetSystemPei/ResetSystemPei.inf
+!endif
+
   FILE FV_IMAGE = 9E21FD93-9C72-4c15-8C4B-E77F1DB2D792 {
     SECTION GUIDED EE4E5898-3914-4259-9D6E-DC7BD79403CF PROCESSING_REQUIRED = TRUE {
       SECTION FV_IMAGE = FVMAIN
-- 
2.18.2

